			<li><a href="#order-tab"><span><?php echo shop_lang('shop:orders:details'); ?></span></a></li>
			<li><a href="#billing-tab"><span><?php echo shop_lang('shop:orders:billing'); ?></span></a></li>
			<li><a href="#delivery-tab"><span><?php echo shop_lang('shop:orders:shipping'); ?></span></a></li>
			<li><a href="#contents-tab"><span><?php echo shop_lang('shop:orders:items'); ?></span></a></li>
			<li><a href="#message-tab"><?php echo shop_lang('shop:orders:messages'); ?></a></li>
			<li><a href="#transactions-tab"><span><?php echo shop_lang('shop:orders:transactions'); ?></span></a></li>
			<li><a href="#notes-tab"><span><?php echo shop_lang('shop:orders:notes'); ?></span></a></li>
			<li><a href="#actions-tab"><span><?php echo shop_lang('shop:orders:actions'); ?></span></a></li>