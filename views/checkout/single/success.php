<div class="SF_PaymentPage">
	<h2 id=""><?php lang('payments');?></h2>
	
	<div class="SF_PaymentInfo">
		<p>
			<?php echo lang('success_order_paid');?>
		</p>
		<p>
			{{ shop:uri to='shop' text='Continue Shopping...' }}
		</p>
	</div>
</div>
