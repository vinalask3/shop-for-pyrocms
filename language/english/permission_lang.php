<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['shop:role_products'] = 'Manage products';
$lang['shop:role_orders'] 	= 'Manage orders';
$lang['shop:role_pgroup'] = 'Manage Price groups';
$lang['shop:role_pgroups'] = 'Manage Price groups';
$lang['shop:role_options'] = 'Manage options';
$lang['shop:role_shipping'] = 'Manage shipping';
$lang['shop:role_blacklist'] = 'Manage blacklisting';
$lang['shop:role_gateways'] = 'Manage Payment gateways';

$lang['shop:role_tax'] = 'Manage Tax groups';
$lang['shop:role_brands'] = 'Manage brands';
$lang['shop:role_packages'] = 'Manage packages';
$lang['shop:role_categories'] = 'Manage categories';



//
// Granualr control
//
$lang['shop:role_advanced_products'] = 'Edit Advanced Product Settings';
$lang['shop:role_product_visibility'] = 'Change Product Visibility';

//
// new naming convention
//
$lang['shop:role_admin_create_products'] = 'Create new products';
$lang['shop:role_admin_edit_products'] = 'Edit existing products';
$lang['shop:role_admin_product_seo'] = 'Edit product SEO';
$lang['shop:role_admin_product_options'] = 'Edit product OPTIONS';
$lang['shop:role_developer_fields'] = 'Developer Fields';

$lang['shop:role_admin_user_data'] = 'User-Data fields';
$lang['shop:role_admin_setup'] = 'Shop Setup Area';



///////////////////////////
// Manage products       //
// admin_products        //
// admin_edit_products   //
// admin_copy_products   //
// admin_delete_products //
///////////////////////////









