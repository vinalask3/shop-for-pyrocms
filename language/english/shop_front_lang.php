<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');
/*
 * SHOP for PyroCMS
 * 
 * Copyright (c) 2013, Salvatore Bordonaro
 * All rights reserved.
 *
 * Author: Salvatore Bordonaro
 * Version: 1.0.0.051
 *
 *
 *
 * 
 * See Full license details on the License.txt file
 */
 
/**
 * SHOP			A full featured shopping cart system for PyroCMS
 *
 * @author		Salvatore Bordonaro
 * @version		1.0.0.051
 * @website		http://www.inspiredgroup.com.au/
 * @system		PyroCMS 2.1.x
 *
 */


$lang['save']							=	'Save'; 
$lang['properties']							=	'Properties'; 
$lang['cart_totals']						=	'Cart Totals'; 
$lang['wl:no_product']						=	'Unable to find the product you were looking for'; 
$lang['wishlist_default_error']				=	'An error occured performing this action on the wishlist'; 
$lang['wishlist_add_error']					=	'An error occured adding this item to the wishlist'; 
$lang['wishlist_del_error']					=	'An error occured deleting this item from the wishlist'; 
$lang['wishlist_not_avail']					=	'Sorry this product is no longer available.'; 
$lang['product_not_avail']					=	'Sorry this product is no longer available.'; 


$lang['user_nostock_warning']		=	'This item is not currently in stock...'; 
$lang['per_page']					=	'Show'; 
$lang['order_by']					=	'Order By'; 
$lang['shop_is_disabled']			=	'The Store is currently disabled';

#
# Messages on sample special page
#
$lang['msg_closed_for_maintenance']			=	'This store is closed for maintenance..';
$lang['msg_no_longer_exist']				=	'The product you are looking for does not exist, or is not available.';
$lang['code_sample_page']					=	'Code Sample Page';
$lang['code_sample_page_desc']				=	'This page is a test for the plugins to see if they are working correctly';



#
# Product Alert  messages
#
$lang['info_alert']				=	'Alert:';
$lang['info_almost_gone']		=	'Running out fast!';


# A
$lang['action']					=	'Action';
$lang['add_to_cart']			=	'Add to Cart';
$lang['add_to_wishlist']		=	'Add to Wishlist';
$lang['agreement_field']		=	'Agree to our Terms and Conditions';
$lang['address']				=	'Address';
$lang['addresses']				=	'Addresses';
$lang['address1']				=	'Address 1';
$lang['address2']				=	'Address 2';

$lang['shipping_address1']		=	'Address 1 (Shipping)';
$lang['shipping_address2']		=	'Address 2 (Shipping)';


$lang['already_in_wishlist']	=	'Already in wishlist';


# B
$lang['billing_address']		=	'Billing Address';
$lang['back']					=	'Back';
$lang['back_to_shop']			=	'Back to Shop';
$lang['back_to_cart']			=	'Back to Cart';

$lang['brand']					=	'Brand';
$lang['base_fee']				=	'Base Fee';



# C
$lang['cart']					=	'Cart';
$lang['clear']					=	'Clear';

$lang['city']					=	'City';
$lang['company']				=	'Company';
$lang['country']				=	'Country';


$lang['shipping_city']			=	'City (Shipping)';
$lang['shipping_company']		=	'Company (Shipping)';
$lang['shipping_country']		=	'Country (Shipping)';


$lang['current_price']			=	'Current price';
$lang['customer']				=	'Customer';
$lang['category']				=	'Category';
$lang['customer_title']			=	'Customer Title';

$lang['checkout']				=	'Checkout';

$lang['continue']				=	'Continue';
$lang['congrats']				=	'Congratulations';
$lang['cost']					=	'Cost';










# D
$lang['details']				=	'Details';
$lang['dashboard']				=	'Dashboard';
$lang['description']			=	'Description';
$lang['date']					=	'Date';
$lang['shipping_status']		=	'Shipping Status';
$lang['delete']					=	'Delete';
$lang['discounts']				=	'Discounts';




# E
$lang['enable_email_notifications']  =	'Enable email notifications';
$lang['enable_mini_cart']	   =	'Enable mini-cart';
$lang['edit']				   =	'Edit';
$lang['email']					=	'Email';
$lang['shipping_email']			=	'Email (Shipping)';
$lang['error']					=	'Error';
$lang['existing_customer']		=	'Have an account ?... Login here';


# F
$lang['from']					=	'From';
$lang['filter']					=	'Filter';
$lang['filters']				=	'Filters';
$lang['first_name']				=	'First Name';
$lang['shipping_first_name']	=	'First Name (Shipping)';

# G
$lang['guest_customer']			=	'Guest';
$lang['go']						=	'Go';


# H
$lang['handling']				=	'Handling';
$lang['notloggedin_title']		=	'Register or Login here';



# I
$lang['id']						=	'ID';
$lang['image']					=	'Image';
$lang['images']					=	'Images';
$lang['items']					=	'Items';
$lang['item_added_successfully']=	'Item added successfully';
$lang['item']						=	'Item';




# L
$lang['last_name']				=	'Surname';
$lang['shipping_last_name']		=	'Surname (Shipping)';


# M
$lang['message']				=	'Message';
$lang['messages']				=	'Messages';
$lang['my']						=	'MY'; /*Itentially uppercase*/
$lang['mydashboard']			=	'MY Dashboard'; /*Itentially uppercase*/


# N
$lang['name']					=	'Name';
$lang['new']					=	'New';
$lang['no_items']				=	'No Items to display';
$lang['new_customer_title']		=	'New Customer';
$lang['new_customer_desc']		=	'If you are a new user, you can register now, it only takes a inute and its easy.';
$lang['new_address']			=	'Create a new Address';

# O
$lang['original_price']			=	'Original Price';
$lang['order']					=	'Order';
$lang['orders']					=	'Orders';
$lang['order_total']			=	'Order Total';
$lang['order_details']			=	'Order Details';
$lang['open']					=	'Open';
$lang['order_id']				=	'Order ID';
$lang['option']					=	'Option';
$lang['order_placed_please_pay']=	'Order has been pending, please continue and pay.';


# P
$lang['price']					=	'Price';
$lang['product_description']	=	'Product Description';
$lang['product_code']		   =	'Product Code';

$lang['profile']				=	'Profile';
$lang['payment_status']			=	'Payment Status';
$lang['phone']					=	'Phone';
$lang['payment_method']			=	'Payment Method';
$lang['payment_canceled']		=	'Payment Cancelled';

$lang['payment']				=	'Payment';
$lang['password']				=	'Password';
$lang['postage_method']			=	'Postage Method';
$lang['submit_order']			=	'Submit Order';
$lang['payments']				=	'Payments';


# R
$lang['read_status']			=	'Read_Status';
$lang['reply']					=	'Reply';
$lang['reply_to']				=	'Reply To';
$lang['remove']					=	'Remove';
$lang['register']				=	'Register';
$lang['reset']					=	'Reset';
$lang['rrp']					=	'RRP';


# S
$lang['shop']					=	'Shop';
$lang['status']					=	'Status';
$lang['settings']				=	'Settings';
$lang['subject']				=	'Subject';
$lang['state']					=	'State';
$lang['shipping_state']					=	'State (Shipping)';
$lang['search']					=	'Search';
$lang['send']					=	'Send';
$lang['shipping']				=	'Shipping';
$lang['success']				=	'Success';
$lang['shipping_address']		=	'Shipping Address';


$lang['stock_status_in_stock']			=	'In Stock';
$lang['stock_status_soon_available']	=	'Coming Soon';
$lang['stock_status_unlimited']			=	'Unlimited';
$lang['stock_status_discontinued']		=	'Discontinued';
$lang['stock_status_out_of_stock']		=	'Out Of Stock';
$lang['stock_status_pre_order']			=	'On Pre-Order ';
$lang['stock_status_disabled']			=	'Disabled';

$lang['summary']					=	'Summary';
$lang['success_order_paid']			=	'Your order has successfully been paid..';
$lang['subtotal']				=	'Subtotal';
$lang['step1']					=	'Step 1';
$lang['step2']					=	'Step 2';
$lang['step3']					=	'Step 3';
$lang['step4']					=	'Step 4';
$lang['step5']					=	'Step 5';
$lang['step6']					=	'Step 6';






# T
$lang['tags']=	'Tags';
$lang['total']					=	'Total';
$lang['total_orders']			=	'Total Orders';
$lang['total_items']			=	'Total Items';



# Q
$lang['qty']					=	'Qty';
$lang['quantity']				=	'Quantity';

# U
$lang['user_id']				=	'User ID';
$lang['user_not_auth']			=	'user_not_auth';
$lang['unread_messages']		=	'Unread Messages';
$lang['user_remember']			=	'Remember Me';
$lang['update_cart']			=	'Update Cart';
$lang['user_login_btn']			=	'Login';


$lang['alsoshipping']	=	'Use the same address for Shipping';


# V
$lang['view']					=	'View';
$lang['views']					=	'Views';

# W
$lang['wishlist']				=	'Wishlist';
$lang['wishlist_items']			=	'Wishlist Items';

# Z
$lang['zip']					=	'Zip';
$lang['shipping_zip']					=	'Zip (Shipping)';


