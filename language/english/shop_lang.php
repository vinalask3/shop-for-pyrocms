<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');
/*
 * This file is accessible for both front and back
 *
 * 
 */


//
// Single value
//
$lang['shop:search:results']						=	'Results';


//
// Single value
//
$lang['shop:global:software:label']						=	'Software';
$lang['shop:global:software:name']						=	'SHOP';
$lang['shop:global:software:slogan']					=	'A full featured shopping cart system for PyroCMS';



//
// data Pairs
//
$lang['shop:global:software:version:label']				=	'Version';
$lang['shop:global:software:version:value']				=	'1.0.0.080';